import numpy as np
from predef import *

def create_node(root):
    """
    Divides the node into different nodes.
    
    Parameters:
    -----------
    root: list
        contains all positions, masses and numbers of all bodies and the width and origin of the node.
    
    Returns:
    --------
    node: list
        contains lists containing all information concerning the new nodes, the center of mass, the total mass and the origin of the "root"
    """
    pos = root[0]
    mass = root[1]
    origin = root[2]
    nbody = root[3]                         # numbers of the bodies. use to keep track of which body has what position
    r = root[4]/2                           # 2r = width of square
    dim = origin.shape[0]
    
    node = 2**dim*[[]]
    node.extend([(np.sum(pos.T*mass, axis = 1)/np.sum(mass)).tolist(), np.sum(mass), 2*r])
     
    direction = (np.dot(pos - origin, np.eye(dim))>0)[:,np.newaxis]
    new_origin = np.array([range(2**dim)]).T & 2**np.arange(dim) > 0
    locations = (new_origin^direction).all(axis = 2).T
    
    for i in range(2**dim):
        if nbody[locations[i]].shape[0] > 1:
            node[i] = [pos[locations[i]], mass[locations[i]], origin-r*(new_origin[i]-0.5), nbody[locations[i]], r]
        elif nbody[locations[i]].shape[0] == 1:
            node[i] = [pos[locations[i]].tolist(), float(mass[locations[i]]), int(nbody[locations[i]])]
    
    # return
    return node


def create_root(pos, mass, origin):
    """
    Creates the root of the Barnes-Hut tree.
    
    Parameters:
    -----------
    pos: np.array([N,D])
        all positions of all bodies
    mass: np.array([N,1])
        all masses of all bodies
    origin: np.array([1,D])
        origin of the system
    
    Returns:
    --------
    root: list
        contains lists containing all information concerning the new nodes, the center of mass, the total mass and the origin of the root
    """
    r = np.max(abs(pos - origin))
    nbody = np.arange(mass.shape[0])
    root = create_node([pos, mass, origin, nbody, 2*r])
    
    # return
    return root

def build_tree(root):
    """
    Builds the Barnes-Hut tree recursively.
    
    Parameters:
    -----------
    root: list
        contains lists containing all information concerning the new nodes, the center of mass, the total mass and the origin of the "root"
    
    Returns:
    --------
    node: list
        contains lists containing all information concerning the new nodes, the center of mass, the total mass and the origin of the "root"
    """
    for i in range(len(root) - 3):
        if len(root[i]) == 5:
            root[i] = build_tree(create_node(root[i]))
    
    # return
    return root


def create_tree(pos, mass):
    """
    Creates the Barnes-Hut tree.
    
    Parameters:
    -----------
    pos: np.array([N,D])
        all positions of all bodies
    mass: np.array([N,1])
        all masses of all bodies
    
    Returns:
    --------
    tree: list
        Barnes-Hut tree
    """
    origin = (np.max(pos, axis = 0) + np.min(pos, axis = 0))/2
    root = create_root(pos, mass, origin)
    tree = build_tree(root)
    
    # return
    return tree


def force_node(pos, node, theta, nbody, Mshape):
    """
    Calculate all forces on the bodies at positions pos, by all nodes recursively
    
    Parameters:
    -----------
    pos: np.array([N,D])
        position of the body
    node: list
        all information of the node that (possibly) exerts force on the body
    theta: float
        ratio threshold of width node / distance body - center of mass node
    force: np.array([1,D])
        force on the body
    nbody: int
        ID of the body
    Mshape: np.array([1,2])
        Gives the shape of the force matrix
    
    Returns:
    --------
    force: np.array([N,D])
        forces on all bodies
    U: np.array([N,])
        Potential energy
    """
    force = np.zeros(Mshape)
    U = np.zeros(Mshape[0])
    
    if len(node) == 3:
        if nbody.shape[0] == 1 and node[-1] != nbody:
            force[nbody], U[nbody] = force_cal(pos, node[0], node[1])
            
        elif nbody.shape[0] != 1:
            if nbody[nbody == node[-1]].shape[0] == 0:
                force[nbody], U[nbody] = force_cal(pos, node[0], node[1])
                
            else:
                nbody_for = np.delete(nbody, np.arange(pos.shape[0])[nbody == node[-1]])
                pos_for = np.delete(pos, np.arange(pos.shape[0])[nbody == node[-1]], 0)
                force[nbody_for], U[nbody_for] = force_cal(pos_for, node[0], node[1])
                
    elif len(node) == (2**Mshape[1] + 3):
        d = np.linalg.norm(pos - np.asarray(node[-3]), axis = 1)
        
        nbody_calc = nbody[node[-1]/d < theta]
        if nbody_calc.shape[0] != 0:
            force[nbody_calc], U[nbody_calc] = force_cal(pos[node[-1]/d < theta], node[-3], node[-2])
            
        pos_cont = pos[node[-1]/d >= theta]
        nbody_cont = nbody[node[-1]/d >= theta]
        
        for i in range(len(node) - 3):
            force1, U1 = force_node(pos_cont, node[i], theta, nbody_cont, Mshape)
            force += force1
            U += U1

    # return
    return force, U


def force_cal(pos1 , pos2, m):
    """
    Calculates force on the body at pos1 by the node at pos2.
    
    Parameters:
    -----------
    pos1: np.array([N,D])
        positions of the bodies
    pos2: list
        center of mass of the node
    m: float
        total mass of the node
    
    Returns:
    --------
    F: np.array([N,D])
        force on the body / bodies at pos
    U: np.array([N,])
        Potential energy
    """
    dx = np.asarray(pos2) - pos1
    dr = np.linalg.norm(dx, axis = 1)
    F = ((G*m/(dr**3))*(dx.T)).T
    U = -G*m/dr
    
    # return
    return F, U


def force_barneshut(pos, mass, theta):
    """
    Combines the creation of the Barnes-Hut tree and the force calulation using that tree.
    
    Parameters:
    -----------
    pos: np.array([N,D])
        all positions of all bodies
    mass: np.array([N,1])
        all masses of all bodies
    theta: float
        ratio threshold of width node / distance body - center of mass node

    Returns:
    --------
    force: np.array([N,D])
        all net forces on all bodies
    U: np.array([N,])
        Potential energy
    """
    tree = create_tree(pos, mass)
    force, U = force_node(pos, tree, theta, np.arange(pos.shape[0]), pos.shape)
    
    # return
    return force, U