import random
import string
import numpy as np


M = 5.972e24  # Earth' mass [kg]
Au = 149597870  # Astronomical unit [km]
Y = 365.25*24*3600*11.862  # Year [s]
G = 6.67408e-11*M*(Au*10**(3))**(-3)*Y**2 # Gravitational constant [m**3 * kg**(-1) * s**(-2)]
massSun = 333000 # M


class Body:
    """Class with all information, settings and results of a single simulated body.
    
    Parameters
    ---------- 
    par: dict
        Parameters and settings for single body
    
    Attributes
    ----------
    mass: float
        Body mass
    pos: list
        Initial position
    velo: list
        Initial velocity
    geo: string
        Geometric representation of position and velocity, either cartesian or cylindrical
    colour: string
        Colour to use when visualizing
    id: int
        Unique identification value
    name: string
        Body name
    trajectory: list
        Calculated trajectory over descrete time steps
    times: list
        Descrete time steps for which the trajectory is calculated
    
    Methods
    -------
    replace(attr, value)
        replace existing value of self.attr by value
    add_trajectory(traj)
        Add calculated trajectory
    add_times(times)
        Add discrete time steps for which the trajectory is calculated
    """
    
    def __init__(self, par):
        
        # Mass
        try:
            self.mass = par['mass']
        except:
            self.mass = 1
            print("Mass not given, set to 1")
        
        # Initial position
        try:
            pos = par['pos']
            if isinstance(pos, tuple):
                pos = list(pos)
            elif not isinstance(pos, list):
                raise TypeError("Invalid data type for initial position")
            self.pos = pos
        except:
            raise IOError("Give initial position as input")
        
        # Initial velocity
        try:
            self.velo = par['velo']
        except:
            print("Initial velocity not given, [0, 0] passed instead")
            self.velo = [0, 0]
        
        # Geometric representation of position and velocity, either carthesian or cylindrical
        try:
            self.geo  = par['geo']
        except:
            raise IOError("Define geometrical representation, either 'cartesian' or 'cylindrical'")
        
        # Aphelion
        try:
            self.aphelion = par['aphelion']
        except:
            self.aphelion = None  # to be determined after calculations of trajectory
        
        # Perihelion
        try:
            self.perihelion = par['perihelion']
        except:
            self.perihelion = None # to be determined after calculations of trajectory
        
        # Velocity at aphelion
        try:
            self.velo_aph = par['velo_aph']
        except:
            self.velo_aph = None # to be determined after calculations of trajectory
        
        # Velocity at perihelion
        try:
            self.velo_per = par['velo_per']
        except:
            self.velo_per = None # to be determined after calculations of trajectory
        
        # Colour when visualizing
        try:
            self.colour = par['colour']
        except:
            self.colour = None
        
        # ID (integer value)
        try:
            ID   = par['id']
        except:
            ID = random.choice(range(10000,100000))  # give random integer as id
        if isinstance(ID, int):
            self.id = ID
        else:
            raise TypeError("ID must be int")
        
        # Name
        try:
            self.name = par['name']
        except:
            self.name = "Anonymous"
    
    def replace(self, attr, value):
        """Replace attribute value by new one"""
        setattr(self, attr, value)
    
    def add_trajectory(self, traj):
        """Add calculated trajectory"""
        self.trajectory = traj
    
    def add_times(self, times):
        """Add discrete time steps for which the trajectory is calculated"""
        self.times = times
    
    def add_velocities(self, velocity):
        """Add velocities at each discrete time step"""
        self.velocities = velocity
    
    def add_ellfit_center(self, center):
        """Add center of ellipsoid fitted on trajectory"""
        self.ellfit_center = center
    
    def add_ellfit_phi(self, phi):
        """Add angle with ?-axis of ellipsoid fitted on trajectory"""
        self.ellfit_phi = phi
    
    def add_ellfit_axes(self, axes):
        """Add axes of ellipsoid fitted on trajectory"""
        self.ellfit_axes = axes
    
    def add_kinetic_energy(self, Ekin):
        """Add kinetic energy at each timestep"""
        self.E_kin = Ekin
    
    def add_potential_energy(self, Epot):
        """Add potential energy at each timestep"""
        self.E_pot = Epot

class Resonance_astroids:
    """Class with all information, settings and results of resonance astroid belt."""

    def __init__(self, par):
        
        # Total number of asteroids
        try:
            self.N = par['N']
        except:
            self.N = 10
            print("Total number of particles not given, set to {}".format(self.N))
            
        # Total mass of all asteroids combined
        try:
            self.mass = par['mass']
        except:
            self.mass = 0.012
            print("Total mass of belt not given, set to mass of Moon: {} Earth masses".format(self.mass))
        
        # Individual masses
        self.masses = (self.mass/self.N*np.ones(self.N)).tolist()
        
        # Mass of center object
        try:
            self.masscenter = pas['masscenter']
        except:
            self.masscenter = massSun
            print("Center mass of belt not given, set to mass of Sun: {} Earth masses".format(self.masscenter))
        
        # Belt radius
        try:
            self.R = par['R']
        except:
            self.R = 3.1
            print("Belt radius not given, set to {}".format(self.R))            
        
        # Colour when visualizing
        try:
            self.colour = par['colour']
        except:
            self.colour = None
        
        # ID (integer value)
        try:
            ID   = par['id']
        except:
            ID = random.choice(range(10000,100000))  # give random integer as id
        if isinstance(ID, int):
            self.id = ID
        else:
            raise TypeError("ID must be int")
        
        # Name
        try:
            self.name = par['name']
        except:
            self.name = "Anonymous resonance asteroid belt"
            
        # Reference period
        try:
            self.T_ref = par['T_ref']
        except:
            self.T_ref = 12*365.25*24*3600/Y
            print("Belt reference period not given, set to period of Jupiter {}".format(self.T_ref)) 
    
        # Initial angle
        try:
            self.theta = par['theta']
        except:
            self.theta = 0
            print("Resonance belt initial angle not given, set to {}".format(self.theta))
    
        # Determine periods of asteroids
        self.T = np.linspace(1,self.N,self.N)*self.T_ref
        
        # Determine positions of asteroids
        r = (self.T*np.sqrt(G*self.masscenter)/(2*np.pi))**(2/3)
        x = r*np.cos(self.theta)
        y = r*np.sin(self.theta)
        self.poss = np.transpose(np.asarray([x, y])).tolist()
        
        # Determine velocities of asteroids
        v = np.sqrt(G*self.masscenter/r)
        vx = np.cos(theta+np.pi/2)*v
        vy = np.sin(theta+np.pi/2)*v
        self.velos = np.transpose(np.asarray([vx, vy])).tolist()            
        
        # Define some parameters that the Bodies class needs
        self.pos = [0, 0]
        self.velo = [0, 0]
        self.geo = 'cartesian'

    def add_trajectories(self, trajs):
        """Add trajectories of all asteroids"""
        self.trajectories = trajs

    def add_velocities(self, velocities):
        """Add velocities of all asteroids"""
        self.velocities = velocities
        
class Asteroids:
    """Class with all information, settings and results of an asteroid belt."""
    
    def __init__(self, par):
        
        # Total number of asteroids
        try:
            self.N = par['N']
        except:
            self.N = 1e4
            print("Total number of particles not given, set to {}".format(self.N))
        
        # Total mass of all asteroids combined
        try:
            self.mass = par['mass']
        except:
            self.mass = 0.012
            print("Total mass of belt not given, set to mass of Moon: {} Earth masses".format(self.mass))
        
        # Individual masses
        self.masses = (self.mass/self.N*np.ones(self.N)).tolist()
        
        # Mass of center object
        try:
            self.masscenter = pas['masscenter']
        except:
            self.masscenter = massSun
            print("Center mass of belt not given, set to mass of Sun: {} Earth masses".format(self.masscenter))
        
        # Inner belt radius
        try:
            self.R_min = par['R_min']
        except:
            self.R_min = 2.1
            print("Inner belt radius not given, set to {}".format(self.R_min))
        
        # Outer belt radius
        try:
            self.R_max = par['R_max']
        except:
            self.R_max = 2.1
            print("Outer belt radius not given, set to {}".format(self.R_max))
        
        # Number of stadard deviations in Gaussian distribution
        try:
            self.stds = par['stds']
        except:
            self.stds = 4
            print("Number of stds not given, set to {}".format(self.stds))
        
        # Colour when visualizing
        try:
            self.colour = par['colour']
        except:
            self.colour = None
        
        # ID (integer value)
        try:
            ID   = par['id']
        except:
            ID = random.choice(range(10000,100000))  # give random integer as id
        if isinstance(ID, int):
            self.id = ID
        else:
            raise TypeError("ID must be int")
        
        # Name
        try:
            self.name = par['name']
        except:
            self.name = "Anonymous asteroid belt"

        # Determine positions
        mu = (self.R_max - self.R_min)/2+self.R_min
        sigma = (self.R_max - self.R_min)/self.stds
        r = np.random.normal(mu, sigma, self.N)
        theta = np.random.uniform(0,2*np.pi,self.N)
        x = r*np.cos(theta)
        y = r*np.sin(theta)
        self.poss = np.transpose(np.asarray([x, y])).tolist()

        # Determine velocities
        v = np.sqrt(G*self.masscenter/r)
        vx = np.cos(theta+np.pi/2)*v
        vy = np.sin(theta+np.pi/2)*v
        self.velos = np.transpose(np.asarray([vx, vy])).tolist()

        # Define some parameters that the Bodies class needs
        self.pos = [0, 0]
        self.velo = [0, 0]
        self.geo = 'cartesian'

    def add_trajectories(self, trajs):
        """Add trajectories of all asteroids"""
        self.trajectories = trajs

    def add_velocities(self, velocities):
        """Add velocities of all asteroids"""
        self.velocities = velocities
    
    def add_kinetic_energy(self, Ekin):
        """Add kinetic energy at each timestep"""
        self.E_kin = Ekin
    
    def add_potential_energy(self, Epot):
        """Add potential energy at each timestep"""
        self.E_pot = Epot
    
    def add_times(self, times):
        """Add discrete time steps for which the trajectory is calculated"""
        self.times = times
        
        
class Bodies:
    """Class to extract attribute information from ensemble of bodies.
    
    Parameters
    ----------
    bodies: list of body.Body class objects
        Contains body.Body class objects with initialization parameters
    
    Attributes
    ----------
    masses: list of floats
        Body masses
    poss: list of lists
        Initial positions
    velos: list of lists
        Initial velocities
    geos: list of strs
        Geometric representation of position and velocity, either cartesian or cylindrical
    ids: list of ints
        Unique identification value
    names: list of strs
        Body name
    """
    
    def __init__(self, bodies):
        masses = []
        poss = []
        velos = []
        geos = []
        IDs = []
        names = []
        for obj in bodies:
            masses.append(obj.mass)
            poss.append(obj.pos)
            velos.append(obj.velo)
            geos.append(obj.geo)
            IDs.append(obj.id)
            names.append(obj.name)
        self.masses = masses
        self.poss = poss
        self.velos = velos
        self.geos = geos
        self.ids = IDs
        self.names = names



class Plot:
    """Class with all settings for visual output."""
    
    def __init__(self, sets):
        
        try:
            self.save = sets['save']
        except:
            self.save = False
        
        try:
            self.inds = sets['inds']
        except:
            self.inds = 'all'
        
        try:
            self.traj_style = sets['traj_style']
        except:
            self.traj_style = 'line'

        # Figure properties
        try:
            self.fontsize_title = sets['fontsize_title']
        except:
            self.fontsize_title = 13
            
        try:
            self.fontsize_axislabel = sets['fontsize_axislabel']
        except:
            self.fontsize_axislabel = 12
            
        try:
            self.fontsize_axisticks = sets['fontsize_axisticks']
        except:
            self.fontsize_axisticks = 10
            
        try:
            self.fontsize_legend = sets['fontsize_legend']
        except:
            self.fontsize_legend = 10
        
        try:
            self.error_alpha = sets['error_alpha']
        except:
            self.error_alpha = 0.2



class Settings:
    """Class with all settings regarding the planetary system and visual output.
    
    Parameters
    ----------
    sets: dict
        Settings
    bodies: list of body.Body class objects
        Contains body.Body class objects with initialization parameters
    
    Attributes
    ----------
    origin: list of floats
        Origin of coordinate system
    origin_geo: string
        Geometric representation of origin, either cartesian or cylindrical
    plot: body.Plot class object
        Settings for visual output
    """
    
    def __init__(self, sets, bodies=None):
        
        # Proceed where calculations ended
        try:
            self.proceed = sets['proceed']
        except:
            self.proceed = False
        
        # Number of time steps after which to store progress data
        try:
            self.n_store_progress = int(sets['n_store_progress'])
        except:
            if self.proceed:
                self.n_store_progress = 500
            else:
                pass
        
        # Location of origin
        try:
            origin = sets['origin']
        except:
            self.origin = [0, 0]
        # if a celestial body is chosen as origin, the origin is set to the coordinates of that body,
        # possibly nonzero, and this body is held fixed
        if isinstance(origin, Body):
            if bodies != None:
                self.origin = bodies[bodies.index(origin)].pos
                self.origin_geo = bodies[bodies.index(origin)].geo
            else:
                raise IOError("Pass on list of bodies when celestial body is set as origin")
        elif isinstance(origin, (list, tuple)):
            self.origin = list(origin)
            self.origin_geo = sets['origin_geo']
        
        # Plot settings
        try:
            self.plot = sets['plot']
        except:
            raise IOError("Plot settings not given")
        
        # System settings
        try:
            self.time = sets['time']
        except:
            raise IOError("Time not given")
            
        try:
            self.timestep = sets['timestep']
        except:
            raise IOError("Timestep not given")
        
        try:
            self.dimensions = sets['dimensions']
        except:
            raise IOError("Number of spatial dimensions not given")
        
        try:
            self.barneshut = sets['Barnes-Hut']
        except:
            self.barneshut = True
        
        try:
            self.theta = sets['Theta']
        except:
            self.theta = 0.5
        
        try:
            self.approx = sets['approx']
        except:
            self.approx = False
        
        try:
            self.timesave = sets['save_timestep']
        except:
            self.timesave = 1
        
        try:
            self.startsave = sets['start_save']
        except:
            self.startsave = 0
        
        try:
            if sets['file_name'] == 'none':
                self.continuecalc = False
            else:
                self.continuecalc = sets['continue_calc']
        except:
            self.continuecalc = False
        
        try:
            self.filename = sets['file_name']
        except:
            self.filename = 'none'


        
def check_ids(bodies):
    """Check whether all bodies have unique ID's
    
    Parameters
    ----------
    bodies: list of body.Body class objects
        Contains body.Body class objects with initialization parameters
    """
    
    ids = []
    for body in bodies:
        ids.append(body.id)
    u = np.unique(np.asarray(ids))
    if len(u) == len(bodies):
        return True
    else:
        return False