"""
Does calculations for moving celestial bodies, interacting through gravitational force.
Takes input parameters from execute.ipynb.
"""



import os
import pickle
import shutil
import time
from datetime import datetime
from glob import glob
from objects import *
from plotf import *
from movements import *
from observables import *
import matplotlib.pyplot as plt

print(os.path.dirname(os.path.realpath(__file__)))

def pipeline(bodies, settings):
    """Defines the pipeline for the calculations and saving the data.
    
    Parameters:
    -----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    settings: objects.Settings class object
        Settings for the interacting system and visual output
    
    Returns:
    --------
    bodies: list of objects.Body class objects
        Contains objects.Body class object with initialization parameters and calculated data
    info: objects.Bodies class object
        Attribute information from ensemble of bodies
    settings: objects.Settings class object
        Used settings for the interacting system
    """
    
    start = time.time()
    
    # Check whether all bodies have unique ID
    if not check_ids(bodies):
        raise ValueError("Two or more bodies have the same ID.")
    
    # Combine info about the bodies in one object
    info = Bodies(bodies)
    
    # First delete old temporary data files
    del_temps()
    
    # Save initialization to temp file in temp directory
    now = datetime.now().strftime("%y_%m_%d_%H_%M_%S")
    temp_folder = "/home/student/project3/temp_" + now + "_data"
    os.makedirs(temp_folder)
    save_init(bodies, info, settings, temp_folder)
    
    # Do all calculations!
    print("Calculating...\n")
    bodies = calc(bodies, settings)
    print("Calculations completed!\n\n")
    
    # Save output data to temporary file
    print("Saving...\n")
    temp_file = save_temp(bodies, info, settings, folder=temp_folder)
    print("Temporary data saved\n\n")
    
    # Execution time
    end = time.time()
    if isinstance(bodies[-1], Asteroids):
        print("Time elapsed: {} minutes, for {} bodies and {} asteroids".format(round((end-start)/60, 1), len(bodies)-1, bodies[-1].N))
    else:
        print("Time elapsed: {} minutes, for {} bodies (no asteroids)".format(round((end-start)/60, 1), len(bodies)))
    
    return bodies, info, temp_file, temp_folder



def calc(bodies, settings):
    # Calculate movements
    if settings.proceed:
        pass
    else:
        pass
    positions, velocities, times, E_kin, E_pot = dynamics(bodies, settings.dimensions, settings.timestep, 
                                                          settings.time, settings.barneshut, settings.theta, 
                                                          settings.approx, settings.timesave, settings.startsave,
                                                          settings.continuecalc, settings.filename)
    for i, body in enumerate(bodies):
        if isinstance(body, Body):
            body.add_trajectory((positions.transpose(1,0,2)[i]).tolist())
            body.add_velocities((velocities[i]).tolist())
            body.add_times(times)
            body.add_kinetic_energy(E_kin.T[i])
            body.add_potential_energy(E_pot.T[i])
        elif isinstance(body, Asteroids):
            body.add_trajectories((positions.transpose(1,0,2)[i:]).tolist())
            body.add_velocities((velocities[i:]).tolist())
            body.add_times(times)
            body.add_kinetic_energy(E_kin.T[i:])
            body.add_potential_energy(E_pot.T[i:])
        
        # Fit 3D trajectory to 2D ellipse
#         r, center, phi, axes = fit_ellipse(body.trajectory)
    return bodies



def save_init(bodies, info, settings, folder):
    data = {
        'bodies' : bodies,
        'info' : info,
        'settings' : settings,
        'description' : 'initialization'
    }
    file = folder + "/initialization.pkl"
    
    out_file = open(file, "wb")
    pickle.dump(data, out_file)
    out_file.close()
    return file



def save_temp(bodies, info, settings, folder):
    """
    Save the acquired data to a temporary file.
    
    Parameters
    ----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    info: objects.Bodies class object
        Attribute information from ensemble of bodies
    settings: objects.Settings class object
        Settings for the interacting system
    folder: string
        Path to the folder of the specific run
    """
    
    name = folder + "/temporary.pkl"
    description = 'temporary'
    data = {
        'description': description,
        'bodies': bodies,
        'info': info,
        'settings': settings
    }
    
    out_file = open(name, "wb")
    pickle.dump(data, out_file)
    out_file.close()
    
    return name
    


def save_results(source, name, description, rm_temp_name):
    """Save the acquired data from a temporary file to a lasting file.
    
    Parameters
    ----------
    source : string
        Either path to temporary pickle file or to temporary directory from which to extract the data
    name : string
        Name of the new file
    description : string
        Description of the stored data
    rm_temp_name : string
        Path to the temporary file to replace
    """
    
    # Check whether source is given as file or as directory
    if source[-13:] == 'temporary.pkl':
        folder = source[:-13]
    elif source[-18:] == 'initialization.pkl':
        folder = source[:-18]
    else:
        folder = source
        source = folder + 'temporary.pkl'
    
    # TODO: implement a way to always include the initial positions of all bodies (including the asteroids)
    
    # Check whether new file path is unique
    if name[-4:] != ".pkl":
        name += ".pkl"
    if os.path.isfile(name):
        raise ValueError("File path {} already exists. Define a unique file name".format(name))
    
    in_file = open(source, 'rb')
    data = pickle.load(in_file)
    data['description'] = description
    in_file.close()
    
    out_file = open(name, "wb")
    pickle.dump(data, out_file)
    out_file.close()



def del_temps():
    """Delete all temporary files in directory, characterized by '/temp_' in the path."""
    dir_path = os.path.dirname(os.path.realpath(__file__))
    condition = dir_path + "/temp_*"
    temp_folders = glob(condition)
    for folder in temp_folders:
        shutil.rmtree(folder)
    