# project3_omw


**CHECKLIST**

**Introduction** (1 min) (Max)
* Scientific significance of our research ( = motivation)
* Current research done
* Kirkwood Gaps

**Theory** (2 min) (Max)
Orbit stability
* Equilibirium zones (Lagrange points)
* Orbital resonance: 1) Dependence on orbital period, 2) Orbital period and semi-major axis

**Model** (2 min) (Olaf)
* Assumptions made
* Barnes-Hut: 1) principle, 2) computation times Barnes-Hut v.s. Brute Force ( = perfomance discussion)
* Ellipse fit
* Validation 1) energy conservation ( = correctness check), 2) with solar system 

**Results and Discussion** (4 min) (Wouter)
* Trajectories last two years
* Final locations
* Histogram semi-major axis
* Eccentricity / semi-major axis ( = correctness check)
* Correctness checks: compare to [Kirkwood Gap data](https://en.wikipedia.org/wiki/Kirkwood_gap)
* Performance is reported and discussed briefly

**Further research** (0.5 min) (Max)
* Assumptions
* Time periods, time steps
* Other asteroid belts

**Summary / Conclusion** (0.5 min) (Max)

**Appendix**
* ...