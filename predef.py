from numpy import pi
from objects import *


""" Natural units """
M = 5.972e24  # Earth' mass [kg]
Au = 149597870  # Astronomical unit [km]
Y = 365.25*24*3600*11.862  # Year [s]
G = 6.67408e-11*M*(Au*10**(3))**(-3)*Y**2 # Gravitational constant [m**3 * kg**(-1) * s**(-2)]


""" Solar system """
# Data extracted from NASA: https://nssdc.gsfc.nasa.gov/planetary/.
solar_system = []

Sun_parameters ={
    'mass'      : 333000,
    'aphelion'  : 0,
    'perihelion': 0,
    'velo_aph'  : 0,
    'velo_per'  : 0,
    'pos'       : [0, 0],
    'velo'      : [0, 0],
    'geo'       : 'cartesian',
    'colour'    : 'yellow',
    'id'        : 0,
    'name'      : "Sun"
}
Sun = Body(Sun_parameters); solar_system.append(Sun)

Mercury_parameters ={
    'mass'      : 0.055,
    'aphelion'  : 0.467,
    'perihelion': 0.307,
    'velo_aph'  : 38.86*Y/Au,
    'velo_per'  : 58.98*Y/Au,
    'pos'       : [0.307, 0],
    'velo'      : [0, 58.98*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'gray',
    'id'        : 1,
    'name'      : "Mercury"
}
Mercury = Body(Mercury_parameters); solar_system.append(Mercury)

Venus_parameters ={
    'mass'      : 0.815,
    'aphelion'  : 0.728,
    'perihelion': 0.718,
    'velo_aph'  : 34.79*Y/Au,
    'velo_per'  : 35.26*Y/Au,
    'pos'       : [0.718, 0],
    'velo'      : [0, 35.26*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'darkorange',
    'id'        : 2,
    'name'      : "Venus"
}
Venus = Body(Venus_parameters); solar_system.append(Venus)

Earth_parameters ={
    'mass'      : 1,
    'aphelion'  : 1.017,
    'perihelion': 0.983,
    'velo_aph'  : 29.29*Y/Au,
    'velo_per'  : 30.29*Y/Au,
    'pos'       : [0.983, 0],
    'velo'      : [0, 30.29*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'dodgerblue',
    'id'        : 3,
    'name'      : "Earth"
}
Earth = Body(Earth_parameters); solar_system.append(Earth)

Mars_parameters ={
    'mass'      : 0.107,
    'aphelion'  : 1.666,
    'perihelion': 1.382,
    'velo_aph'  : 21.97*Y/Au,
    'velo_per'  : 26.50*Y/Au,
    'pos'       : [1.382, 0],
    'velo'      : [0, 26.50*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'red',
    'id'        : 4,
    'name'      : "Mars"
}
Mars = Body(Mars_parameters); solar_system.append(Mars)

Jupiter_parameters ={
    'mass'      : 317.8,
    'aphelion'  : 5.4588,
    'perihelion': 4.9501,
    'velo_aph'  : 12.44*Y/Au,
    'velo_per'  : 13.72*Y/Au,
    'pos'       : [4.9501, 0],
    'velo'      : [0, 13.72*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'sandybrown',
    'id'        : 5,
    'name'      : "Jupiter"
}
Jupiter = Body(Jupiter_parameters); solar_system.append(Jupiter)

Saturn_parameters ={
    'mass'      : 95.159,
    'aphelion'  : 10.1238,
    'perihelion': 9.0412,
    'velo_aph'  : 9.09*Y/Au,
    'velo_per'  : 10.18*Y/Au,
    'pos'       : [9.0412, 0],
    'velo'      : [0, 10.18*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'goldenrod',
    'id'        : 6,
    'name'      : "Saturn"
}
Saturn = Body(Saturn_parameters); solar_system.append(Saturn)

Uranus_parameters ={
    'mass'      : 14.536,
    'aphelion'  : 20.11,
    'perihelion': 18.33,
    'velo_aph'  : 6.49*Y/Au,
    'velo_per'  : 7.11*Y/Au,
    'pos'       : [18.33, 0],
    'velo'      : [0, 7.11*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'skyblue',
    'id'        : 7,
    'name'      : "Uranus"
}
Uranus = Body(Uranus_parameters); solar_system.append(Uranus)

Neptune_parameters ={
    'mass'      : 17.147,
    'aphelion'  : 30.33,
    'perihelion': 29.81,
    'velo_aph'  : 5.37*Y/Au,
    'velo_per'  : 5.50*Y/Au,
    'pos'       : [29.81, 0],
    'velo'      : [0, 5.50*Y/Au],
    'geo'       : 'cartesian',
    'colour'    : 'blue',
    'id'        : 8,
    'name'      : "Neptune"
}
Neptune = Body(Neptune_parameters); solar_system.append(Neptune)

# Asteroid belt, data from: http://astronomy.swin.edu.au/~gmackie/SAO/HET602_Amaya/m14a01.ppt
Asteroid_belt_parameters = {
    'N'      : 50,  # number of asteroids
    'mass'   : 0.012,  # total mass of all asteroids combined
    'R_min'  : 2.1,  # lower bound orbital radius from the Sun
    'R_max'  : 4.1,  # upper bound orbital radius from the Sun
    'stds'   : 4,  # number of standard deviations in Gaussian distribution that fit between R_min and R_max
    'colour' : 'lightgray',
    'id'     : 10,
    'name'   : 'Asteroid Belt'
}
Asteroid_belt = Asteroids(Asteroid_belt_parameters); solar_system.append(Asteroid_belt)

# Resonance Astroid belt
Resonance_belt_parameters = {
    'N'      : 5000,  # number of asteroids
    'mass'   : 0.012,  # total mass of all asteroids combined
    'R'      : 3.1,  # orbital radius from the Sun
    'theta'  : 0,
    'colour' : 'lightgray',
    'id'     : 10,
    'name'   : 'Resonance Asteroid Belt'   
}
# Resonance_belt = Resonance_astroids(Resonance_belt_parameters); solar_system.append(Resonance_belt)

""" Quick writing """
All = 'all'
yes = True
no = False
line = 'line'
scatter = 'scatter'