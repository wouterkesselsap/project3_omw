import numpy as np
from numpy.linalg import eig, inv

def create_ellipse(A, c, phi, noise, N_points):
    """
    Function to create a 2D or 3D ellipse for testing prepare_ellipse and fit_ellipse function.
    
    Parameters
    ----------
    A : (3,) np.array
        (x,y,z) amplitudes
    c : (3,) np.array
        (x,y,z) centroid coordinates
    phi : (3,) np.array
        Rotation angles along (x,y,z)-axis
    noise: float
        Relative noise level in x and y direction
    N_points : float
        Number of data points to create
        
    Returns
    -------
    r : (,3) np.array
        (x,y,z) coordinates of ellipse
    """
    
    # Define relative noise
    noise = np.max(A)*noise
    
    # Create standard ellipse
    R = np.arange(0,2*np.pi, 2*np.pi/N_points)
    r = [A[0]*np.cos(R) + noise*np.random.rand(len(R)),
         A[1]*np.sin(R) + noise*np.random.rand(len(R)),
         0*R]
    
    if len(A) == 3 and len(c) == 3 and len(phi) == 3:
        # Rotate ellipse
        r = rot_x(r,phi[0])
        r = rot_y(r,phi[1])
        r = rot_z(r,phi[2])
        # Translate ellipse
        r[0] = r[0] + c[0]
        r[1] = r[1] + c[1]
        r[2] = r[2] + c[2]
        
    elif len(A) == 2 and len(c) == 2 and len(phi) == 1:
        # Rotate ellipse
        r = rot_z(r,phi)
        # Translate ellipse
        r[0] = r[0] + c[0]
        r[1] = r[1] + c[1]
        r = [r[0], r[1]]
    else:
        raise ValueError('Dimensions of A, c and phi are not compatible. Change dimensions of A, c and/or phi.')
    
    r = np.asarray(r)

    return r

def fit_ellipse(r_input):
    """
    Function to fit 3D or 2D ellipse.
    Inspired by 2D version created by http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    
    Parameters
    ----------
    r_input : (2,) or (3,) np.array
        (x,y,z) coordinates of ellipse

    Returns
    -------
    r : (2,) np.array
        (x,y) coordinates of ellipse after rotation
    center : (2,) np.array
        (x,y) center coordinates of ellipse
    phi : (2,) np.array
        angle of ellipse
    axes : (2,) np.array
        axis length of ellipse
    """
    
    # Convert to 2D if necessary
    if np.shape(r_input)[0] == 3:
        r = prepare_ellipse(r_input)
    elif np.shape(r_input)[0] == 2:
        r = r_input
    else:
        raise ValueError('Ellipse trajectory is not 2D nor 3D.')
        
    # Fit ellipse
    a = fit_ellipse_data(r[0],r[1])
    center = ellipse_center(a)
    phi = ellipse_angle_of_rotation(a)
    axes = ellipse_axis_length(a)

    return r, center, phi, axes

def prepare_ellipse(r):
    """
    Function to prepare 3D ellipse for 2D fit.
    
    Parameters
    ----------
    r_input : (3,) np.array
        (x,y,z) coordinates of ellipse
        
    Returns
    -------
    r : (2,) np.array
        (x,y) coordinates of ellipse after rotation
    """
    
    # Calculate centroid and translate to center
    c = np.mean(r,0)
    r = r - c
    
    # Rotate along y-axis
    phi_xz = np.arctan(r[2]/r[0])
    r = rot_y(r, phi_xz)
    
    # Rotate along x-axis
    phi_yz = np.arctan(r[2]/r[1])
    r = rot_x(r, phi_yz)
    
    # Translate center to original centroid.
    r = r + c

    return r

def fit_ellipse_data(x,y):
    """
    Function to fit 2D trajectory data to ellipse.
    Source: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    
    Parameters
    ----------
    x : (2,) np.array
        x data points
    y : (2,1) np.array
        y data points 
        
    Returns
    -------
    a : (5,) np.array
        Description of fitted ellipse
        a_{x,x}, a_{x,y},a_{y,y},a_{x},a_{y},a_1
    """
    
    x = x[:,np.newaxis]
    y = y[:,np.newaxis]
    D =  np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    S = np.dot(D.T,D)
    C = np.zeros([6,6])
    C[0,2] = C[2,0] = 2; C[1,1] = -1
    E, V =  eig(np.dot(inv(S), C))
    n = np.argmax(np.abs(E))
    a = V[:,n]
    
    return a

def ellipse_center(a):
    """
    Function to find the center of an ellipse.
    Source: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    
    Parameters
    ----------
    a : (5,1) np.array
        Description of fitted ellipse
        a_{x,x}, a_{x,y},a_{y,y},a_{x},a_{y},a_1
        
    Returns
    -------
    np.array([x0,y0])
        (x,y)-position of center of ellipse
    
    """
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    num = b*b-a*c
    x0=(c*d-b*f)/num
    y0=(a*f-b*d)/num
    return np.array([x0,y0])


def ellipse_axis_length(a):
    """
    Function to calculate the lenghts of the axes of an ellipse.
    Source: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    
    Parameters
    ----------
    a : (5,1) np.array
        Description of fitted ellipse
        a_{x,x}, a_{x,y},a_{y,y},a_{x},a_{y},a_1
        
    Returns
    -------
    np.array([res1, res2])
        Axis lengths
    """  
    
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    down1=(b*b-a*c)*( (c-a)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    down2=(b*b-a*c)*( (a-c)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    res1=np.sqrt(up/down1)
    res2=np.sqrt(up/down2)
    return np.array([res1, res2])

def ellipse_angle_of_rotation(a):
    """
    Function to calculate the angle of rotation of an ellipse.
    Source: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    
    Parameters
    ----------
    a : (5,1) np.array
        Description of fitted ellipse
        a_{x,x}, a_{x,y},a_{y,y},a_{x},a_{y},a_1
        
    Returns
    -------
    0, np.pi/2, np.arctan(2*b/(a-c))/2 or np.pi/2 + np.arctan(2*b/(a-c))/2 : float
        Angle of rotation
    """
    
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    if b == 0:
        if a > c:
            return 0
        else:
            return np.pi/2
    else:
        if a > c:
            return np.arctan(2*b/(a-c))/2
        else:
            return np.pi/2 + np.arctan(2*b/(a-c))/2

def eccentricity(axes):
    # determine semi-minor and semi-major axis
    semi_minor_axis = np.min(axes)
    semi_major_axis = np.max(axes)
    # calculate eccentricity
    e = np.sqrt(1-semi_minor_axis**2/semi_major_axis**2)
    
    if e > 1:
        raise ValueError('Eccentricity =',e,' but should be smaller than 1.')
    
    return semi_minor_axis, semi_major_axis, e
        
        
def rot_x(r, theta):
    """
    Function to rotate coordinates parallel to x axis
    
    Parameters
    ----------
    r : (3,) np.array
        (x,y,z) initial coordinates
    theta : float
        Rotate counterclockwise along axis with angle theta, in radials
        
    Returns
    -------
    r : (3,) np.array
        (x,y,z) rotated coordinates
    """
    
    x_r = r[0]
    y_r = r[1]*np.cos(theta) - r[2]*np.sin(theta)
    z_r = r[1]*np.sin(theta) + r[2]*np.cos(theta)
    r = [x_r, y_r, z_r]
    return r

def rot_y(r, theta):
    """
    Function to rotate coordinates parallel to y-axis
    
    Parameters
    ----------
    r : (3,) np.array
        (x,y,z) initial coordinates
    theta : float
        Rotate counterclockwise along axis with angle theta, in radials
        
    Returns
    -------
    r : (3,) np.array
        (x,y,z) rotated coordinates
    """
    
    x_r = r[0]*np.cos(theta) + r[2]*np.sin(theta)
    y_r = r[1]
    z_r = -r[0]*np.sin(theta)+ r[2]*np.cos(theta)
    r = [x_r, y_r, z_r]
    return r

def rot_z(r, theta):
    """
    Function to rotate coordinates parallel to y axis
    
    Parameters
    ----------
    r : (3,) np.array
        (x,y,z) initial coordinates
    theta : float
        Rotate counterclockwise along axis with angle theta, in radials
        
    Returns
    -------
    r : (3,) np.array
        (x,y,z) rotated coordinates
    """
    
    x_r = r[0]*np.cos(theta) - r[1]*np.sin(theta)
    y_r = r[0]*np.sin(theta) + r[1]*np.cos(theta)
    z_r = r[2]
    r = [x_r, y_r, z_r]
    return r

def orbital_period_kepler(semi_major_axis,G,M):
    """
    Function to calculate orbital period using Kepler's Third Law.
    
    Parameters
    ----------
    semi_major_axis : float
        Orbit's semi-major axis
    G : float
        Gravitational constant
    M : float
        Mass of the more massive body
    
    Returns
    -------
    T : float
        Orbital period
    """
    
    T = 2*np.pi*np.sqrt(semi_major_axis**3/(G * M))
    
    return T