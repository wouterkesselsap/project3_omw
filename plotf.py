import pickle
import numpy as np
import matplotlib.pyplot as plt
from objects import *



def plots(file, settings_passed):
    """This function calls all the actual plotting and visualization functions.
    This is to keep execute.ipynb short.
    
    Parameters
    ----------
    file : string
        Path of the pickle file to read data from
    settings_passed : objects.Settings class object
        Settings for the interacting system and visual output
    """
    
    # Check whether file extension is given
    if file[-4:] != ".pkl":
        file += ".pkl"
    
    with open(file, 'rb') as f:
        data = pickle.load(f)
    f.close()
    description = data['description']
    bodies = data['bodies']
    info = data['info']
    settings_saved = data['settings']
    
    # Trajectories
    trajectory(bodies, settings_passed, inds=settings_passed.plot.inds)
    # Energies
    plot_energy(bodies, settings_passed)
    
#     plot_eccentricity(semi_major_axis, e)


def trajectory(bodies, settings, inds='all'):
    """Plot the trajectory of the bodies.
    
    Parameters
    ----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    settings: objects.Settings class object
        Settings for the interacting system
    inds : string, or list, or array-like, or int
        Indeces of bodies in 'bodies' to plot
    """
    
    if inds == 'all':
        inds = range(len(bodies))
    else:
        inds = list(inds)
    
    size = lambda x : int(40*2/(1+np.exp(-0.05*x))-1)  # initial position plot size corresponding to mass
    fig = plt.figure()
    # Loop over only the bodies specified by indeces list inds
    for i in inds:
        # Set colour
        if isinstance(bodies[i].colour, str):
            col = bodies[i].colour
        else:
            col = 'black'
        
        # Plot
        if isinstance(bodies[i], Body):
            # Positions
            x = np.asarray(bodies[i].trajectory)[:,0]
            y = np.asarray(bodies[i].trajectory)[:,1]
            if settings.plot.traj_style == 'line':
                plt.plot(x, y, color=col, label=bodies[i].name)
                plt.scatter(x[0], y[0], color=col, s=size(bodies[i].mass))
            elif settings.plot.traj_style == 'scatter':
                plt.scatter(x, y, color=col, label=bodies[i].name)
        elif isinstance(bodies[i], Asteroids):
            for num, asteroid in enumerate(bodies[i].trajectories):
                x = np.asarray(asteroid)[:,0]
                y = np.asarray(asteroid)[:,1]
                if num == 0:
                    plt.plot(x, y, color=col, lw=1, label='Asteroids')
                else:
                    plt.plot(x, y, color=col, lw=1)
                plt.scatter(x[0], y[0], color=col, s=size(bodies[i].masses[num]))
                
    plt.xlabel("x $[Au]$", fontsize = settings.plot.fontsize_axislabel)
    plt.ylabel("y $[Au]$", fontsize = settings.plot.fontsize_axislabel)
    plt.axis("equal")
    plt.title("Trajectories during $T = {} [y_J]$".format(np.max(bodies[inds[0]].times)),
              fontsize = settings.plot.fontsize_title)
    plt.legend(loc='lower right', fontsize = settings.plot.fontsize_legend)
    plt.tick_params(labelsize = settings.plot.fontsize_axisticks)
    plt.show()
        
    
    if settings.plot.save:
        pass
    pass

def plot_eccentricity(semi_major_axis, e, settings):
    # plot eccentricty to semi_major_axis
    plt.figure()
    plt.plot(semi_major_axis,e,'b.')
    plt.title('Astroid distribution', fontsize = settings.plot.fontsize_title)
    plt.xlabel('Semi-major Axis [AU]', fontsize = settings.plot.fontsize_axislabel)
    plt.ylabel('Eccentricity [-]', fontsize = settings.plot.fontsize_axislabel)
    plt.tick_params(labelsize = settings.plot.fontsize_axisticks)
    plt.show()
    

def plot_energy(bodies, settings):
    """
    Plot energy of the bodies.
    
    Parameters
    ----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    settings: objects.Settings class object
        Settings for the interacting system
    """
    t = bodies[0].times
    E_kin = np.zeros((len(t)))
    E_pot = np.zeros((len(t)))
    for i in range(len(bodies)):
        if isinstance(bodies[i], Asteroids):
            if bodies[i].N >1:
                E_kin += np.sum(np.asarray(bodies[i].E_kin), axis = 0)
                E_pot += np.sum(np.asarray(bodies[i].E_pot), axis = 0)
            elif bodies[i].N == 1:
                E_kin += np.asarray(bodies[i].E_kin)
                E_pot += np.asarray(bodies[i].E_pot)
        else:
            E_kin += np.asarray(bodies[i].E_kin)
            E_pot += np.asarray(bodies[i].E_pot)
    
    E_kin_std = np.std(E_kin)
    E_pot_std = np.std(E_pot)
    E_std = np.std(E_kin + E_pot)
    
    fig = plt.figure()
    
    plt.plot(t, E_kin, color = 'red', label = 'Kinetic energy')
    plt.fill_between(t, E_kin - E_kin_std, E_kin + E_kin_std, alpha = settings.plot.error_alpha, color = 'red')
    
    plt.plot(t, E_pot, color = 'blue', label = 'Potential energy')
    plt.fill_between(t, E_pot - E_pot_std, E_pot + E_pot_std, alpha = settings.plot.error_alpha, color = 'blue')
    
    plt.plot(t, E_kin + E_pot, color = 'green', label = 'Total energy')
    plt.fill_between(t, E_kin + E_pot - E_std, E_kin + E_pot + E_std, alpha = settings.plot.error_alpha, color = 'green')
    
    plt.xlabel('time $[y_J]$', fontsize = settings.plot.fontsize_title)
    plt.ylabel('Energy $[\\frac{Au^2}{Y^2}]$', fontsize = settings.plot.fontsize_title)
    plt.title('Energy')
    plt.legend(loc='upper right', fontsize = settings.plot.fontsize_legend)
    plt.show()
    

    

def save_figure(figure, name):
    pass