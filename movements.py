import numpy as np
import warnings
from predef import *
from barnes_hut import *
import math
import pickle


def extract_data(bodies, D):
    """
    Extracts data from class bodies necessary to calculate the trajectories of the bodies.
    
    Parameters:
    -----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    D: float
        number of spatial dimensions
    
    Returns:
    --------
    positions: np.array([N,D])
        positions of all bodies
    velocities: np.array([N,D])
        velocities of all bodies
    masses: np.array([N,1])
        masses of all bodies
    n: float
        number of non-asteroids
    """
    masses = []
    positions = []
    velocities = []
    n = 0
    for i in range(len(bodies)):
        if not isinstance(bodies[i], Asteroids):
            masses.append(bodies[i].mass)
            n += 1

            if bodies[i].geo == 'cylindrical':
                r = bodies[i].pos[0]
                theta = bodies[i].pos[1]
                dr = bodies[i].velo[0]
                dtheta = bodies[i].velo[1]

                pos = [r*np.cos(theta),r*np.sin(theta)]            
                vel = [dr*np.cos(theta) - r*dtheta*np.sin(theta), dr*np.sin(theta) + r*dtheta*np.cos(theta)]
                if D == 3:
                    pos.append(bodies[i].pos[2])
                    velo.append(bodies[i].velo[2])

                positions.append(pos)
                velocities.append(vel)
            elif bodies[i].geo == 'spherical' and D == 3:
                r = bodies[i].pos[0]
                theta = bodies[i].pos[1]
                phi = bodies[i].pos[2]
                dr = bodies[i].velo[0]
                dtheta = bodies[i].velo[1]
                dphi = bodies[i].velo[2]

                pos = [r*np.sin(theta)*np.cos(phi), r*np.sin(theta)*np.cos(theta), r*np.cos(theta)]
                vel = [dr*np.sin(theta)*np.cos(phi) + r*dtheta*np.cos(theta)*np.cos(phi) - r*dphi*np.sin(theta)*np.sin(phi),
                      dr*np.sin(theta)*np.sin(phi) + r*dtheta*np.cos(theta)*np.sin(phi) + r*dphi*np.sin(theta)*np.cos(theta),
                      dr*np.cos(theta) - r*dtheta*np.sin(theta)]

                positions.append(pos)
                velocities.append(vel)

            elif bodies[i].geo == 'cartesian':
                if D<=len(bodies[i].pos) or D <= len(bodies[i].velo):
                    positions.append(bodies[i].pos[0:D])
                    velocities.append(bodies[i].velo[0:D])
                else:
                    raise ValueError("Number of dimensions does not match input")
            else:
                raise ValueError("Error in extract_data. Check geo and number of dimensions.")
        elif isinstance(bodies[i], Asteroids):
            masses.extend(bodies[i].masses)
            positions.extend(bodies[i].poss)
            velocities.extend(bodies[i].velos)
            
    N = len(masses)        
    masses = np.asarray(masses)
    positions = np.asarray(positions).reshape(N, D)
    velocities = np.asarray(velocities).reshape(N, D)
    
    # return
    return positions, velocities, masses, n


def extract_file_data(file):
    """
    Extracts data from a file to continue calculations.
    
    Parameters
    ----------
    file : string
        Path of the pickle file to read data from
    
    Returns:
    --------
    
    pos: np.array([N,D])
        all final postions from previous calculations
    velo: np.array([N,D])
        all final velocities from previous calculations
    mass: np.array([N,])
        all masses of all bodies
    n: float
        number of non-asteroids
    t: float
        final time of previous calculations
    barnes_hut: bool
        use the Barnes-Hut algorithm or not
    theta: float
        ratio in Barnes-Hut algorithm
    approximation: bool
        use approximation or not
    """
    # Check whether file extension is given
    if file[-4:] != ".pkl":
        file += ".pkl"
    
    with open(file, 'rb') as f:
        data = pickle.load(f)
    f.close()
    bodies = data['bodies']
    settings_saved = data['settings']
    
    barnes_hut = settings_saved.barneshut
    theta = settings_saved.theta
    approximation = settings_saved.approx
    
    t = bodies[0].times[-1]
    velo = []
    pos = []
    mass = []
    n = 0
    for i in range(len(bodies)):
        if isinstance(bodies[i], Asteroids):
            if bodies[i].N >1:
                traj = np.asarray(bodies[i].trajectories)
                traj = np.transpose(traj, (1,0,2))
                pos.extend((traj[-1]).tolist())
                velo.extend(bodies[i].velocities)
                mass.extend(bodies[i].masses)
            elif bodies[i].N == 1:
                pos.append(bodies[i].trajectories[-1])
                velo.append(bodies[i].velocities)
                mass.append(bodies[i].masses)
        else:
            velo.append(bodies[i].velocities)
            pos.append(bodies[i].trajectory[-1])
            mass.append(bodies[i].mass)
            n += 1
    
    pos = np.asarray(pos)
    velo = np.asarray(velo)
    mass = np.asarray(mass)
    #return
    return pos, velo, mass, n, t, barnes_hut, theta, approximation


def compatibility_check(n, bodies, n_asteroids):
    """
    Checks whether the calculations can be continued and processed succesfully.
    
    Parameters
    ----------
    n: int
        total number of bodies in saved data
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    n_asteroids: int
        total number of asteroids in saved data
    """
    nbodies = 0
    non_ast = 0
    for i in range(len(bodies)):
        if isinstance(bodies[i], Asteroids):
            nbodies += bodies[i].N
        else:
            nbodies += 1
    
    if nbodies != n:
        print("Total number of bodies in saved data: ", n, ", of which", n_asteroids, "are asteroids.")
        raise ValueError("Check if number of bodies in predef.py is the same as in previous calculations")
            
    
def force_calc(mass, positions, D):
    """
    Calculates all forces exerted on each body using "brute force" algorithm.
    
    Parameters:
    -----------
    mass: np.array([N,1])
        masses of all bodies
    positions: np.array([N,D])
        positions of all bodies
    D: float
        number of spatial dimensions
    
    Returns:
    --------
    F: np.array([N,D])
        force exerted on each body
    U: np.array([N,])
        Potential energy
    """
    N = mass.shape[0]
    m = (mass * np.ones((N,N))).T
    
    x = positions*np.ones([N,N,D])
    dx = np.transpose(x, (1,0,2)) - x
    dr_mag = np.linalg.norm(dx, axis = 2)
    dr_mag += np.eye(N)
    
    U = -np.sum(G*m*(1 - np.eye(N))/dr_mag, axis = 1)
    F_mag = G*m*(1 - np.eye(N))/(dr_mag**3)
    F = np.transpose(dx, (2,0,1))*F_mag
    F = np.sum(F, axis = 1)                                             # All forces on all particles ([D,N] matrix)
    F = F.transpose()                                                   # Transpose matrix to obtain desired [N,D] matrix
    # return
    return F, U


def velocity(v0, f0, f1, h):
    """
    Calculates velocities of all bodies at next timestep.
    
    Parameters:
    -----------
    v0: np.array([N,D])
        velocities of all bodies at time 0
    f0: np.array([N,D])
        forces on all bodies at time 0
    f1: np.array([N,D])
        forces on all bodies at time 1
    h: float
        timestep
    
    Returns:
    --------
    v1: np.array([N,D])
        velocities of all bodies at time 1
    """
    v1 = v0 + (h/2)*(f1 + f0)
    
    # return
    return v1


def position(x0, v0, f, h):
    """
    Calculates positions of all bodies at next timestep.
    
    Parameters:
    -----------
    x0: np.array([N,D])
        positions of all bodies at time 0
    v0: np.array([N,D])
        velocities of all bodies at time 0
    f: np.array([N,D])
        forces on all bodies at time 0
    h: float
        timestep
    
    Returns:
    --------
    x1: np.array([N,D])
        positions of all bodies at time 1
    """
    x1 = x0 + h*v0 + ((h**2)/2)*f
    
    # return
    return x1


def force_asteroids(mass, asteroids, positions, D):
    """
    Calculates all forces exerted on each body using "brute force" algorithm.
    
    Parameters:
    -----------
    mass: np.array([n,1])
        masses of all planets and the sun
    asteroids: np.array([N,D])
        positions of all asteroids
    positions: np.array([n,D])
        positions of all planets and the sun
    D: float
        number of spatial dimensions
    
    Returns:
    --------
    F: np.array([N,D])
        force exerted on each body
    U: np.array([N,])
        Potential energy
    """
    n = mass.shape[0]
    N = asteroids.shape[0]
    m = mass * np.ones((N,n)) 
    
    x = asteroids*np.ones([n,N,D])
    x = np.transpose(x, (1,0,2))                                        # np.array([N,n,D])
    
    dx = positions - x
    dr = np.linalg.norm(dx, axis = 2)

    F_mag = G*m/(dr**3)
    U = -np.sum(G*m/dr, axis = 1)
    F = np.transpose(dx, (2,0,1))*F_mag
    F = np.sum(F, axis = 2)                                             # All forces on all particles ([D,N] matrix)
    F = F.transpose()                                                   # Transpose matrix to obtain desired [N,D] matrix
    # return
    return F, U

def force_approx(mass, positions, D, n):
    """
    Combines the forces on asteroids and on non-asteroids.
    
    Parameters:
    -----------
    mass: np.array([N,])
        all masses
    positions: np.array([N,D])
        all positions
    D: float
        dimensions
    n: float
        number of non-asteroid bodies
    
    Returns:
    --------
    Force: np.array([N,D])
        force exerted on each body
    U: np.array([N,])
        Potential energy
    """
    Force = np.zeros([mass.shape[0],D])
    U = np.zeros(mass.shape[0])
    Force[:n], U[:n] = force_calc(mass[:n], positions[:n], D)
    Force[n:], U[n:] = force_asteroids(mass[:n], positions[n:], positions[:n], D)
    
    # return
    return Force, U


def dynamics(bodies, D, h, t_max, barnes_hut, theta, approximation, j, startsave, continue_calc, filename = 'none'):
    """
    Calculates the dynamics (trajectories) of all bodies
    
    Parameters:
    -----------
    bodies: list of objects.Body class objects
        Contains objects.Body class objects with initialization parameters
    D: float
        number of spatial dimensions
    h: float
        timestep
    t_max: float or int
        Total time of the simulated trajectories
    barnes_hut: bool
        states if the Barnes-Hut algorithm should be used or not
    theta: float
        ratio used in the Barnes-Hut algorithm
    approximation: bool
        states if only interaction on asteroids and not of the asteroids should be taken
    j: int
        every j_th timestep the data is saved
    startsave: int
        start saving at this timestep
    continue_calc: bool
        states whether to continue on previous calculation or not
    filename: string
        name of the file of previous calculations
    
    Returns:
    --------
    all_pos_corr: np.array([T+1,N,D])
        all positions of all bodies at all timesteps, corrected for the position of the sun
    velo: np.array([N,D])
        all velocities of all bodies at all timesteps
    times: list
        all times of the simulated points of the trajectories
    E_kin: np.array([T+1,N])
        kinetic energy
    E_pot: np.array([T+1,N])
        potential energy
    """
    if barnes_hut and approximation:
        raise ValueError("Can't combine Barnes-Hut with our approximation. Set at least one to false")
        
    T = round(t_max/h)
    if continue_calc:
        positions, velo, mass, n, t_init, barnes_hut, theta, approximation = extract_file_data(filename)
        compatibility_check(mass.shape[0], bodies, mass.shape[0] - n)
    elif not continue_calc:
        positions, velo, mass, n = extract_data(bodies, D)
        t_init = 0
    
    if barnes_hut and theta != 0 and not approximation:
        force0, U = force_barneshut(positions, mass, theta)
    elif approximation and not barnes_hut:
        force0, U = force_approx(mass, positions, D, n)
    else:
        force0, U = force_calc(mass, positions, D)
    
    all_pos = np.zeros((math.ceil((T-startsave+1)/j), positions.shape[0], positions.shape[1]))
    E_kin = np.zeros((math.ceil((T-startsave+1)/j), positions.shape[0]))
    E_pot = np.zeros((math.ceil((T-startsave+1)/j), positions.shape[0]))
    times = []
    
    index = -1
    if startsave == 0:
        index += 1
        times.append(t_init)
        E_kin[index] = 0.5*(np.linalg.norm(velo, axis=1)**2)
        E_pot[index] = U
        all_pos[index] = positions
    
    if barnes_hut and theta != 0 and not approximation:
        for i in range(1,T+1):
            positions = position(positions, velo, force0, h)
            force1, U = force_barneshut(positions, mass, theta)
            velo = velocity(velo, force0, force1, h)
            force0 = force1
            if (T+1)>=10:
                if i%round((T+1)/10) == 0:
                    print(int(100*round(i/(T+1), 1)), "%")
            if i%j == 0 and i>=startsave:
                index += 1
                all_pos[index] = positions
                E_kin[index] = 0.5*(np.linalg.norm(velo, axis=1)**2)
                E_pot[index] = U
                times.append(h*i + t_init)
    elif approximation and not barnes_hut:
        for i in range(1,T+1):
            positions = position(positions, velo, force0, h)
            force1, U = force_approx(mass, positions, D, n)
            velo = velocity(velo, force0, force1, h)
            force0 = force1
            if (T+1)>=10:
                if i%round((T+1)/10) == 0:
                    print(int(100*round(i/(T+1), 1)), "%")
            if i%j == 0 and i>=startsave:
                index += 1
                all_pos[index] = positions
                E_kin[index] = 0.5*(np.linalg.norm(velo, axis=1)**2)
                E_pot[index] = U
                times.append(h*i + t_init)
    else:
        for i in range(1,T+1):
            positions = position(positions, velo, force0, h)
            force1, U = force_calc(mass, positions, D)
            velo = velocity(velo, force0, force1, h)
            force0 = force1
            if (T+1)>=10:
                if i%round((T+1)/10) == 0:
                    print(int(100*round(i/(T+1), 1)), "%")
            if i%j == 0 and i>=startsave:
                index += 1
                all_pos[index] = positions
                E_kin[index] = 0.5*(np.linalg.norm(velo, axis=1)**2)
                E_pot[index] = U
                times.append(h*i + t_init)
    
    all_pos_corr = np.transpose(np.transpose(all_pos, (1,0,2)) - np.transpose(all_pos, (1,0,2))[0], (1,0,2))

    # return
    return all_pos_corr, velo, times, E_kin, E_pot
        